#include<iostream>
#include<cmath>

using namespace std;

const int MAXN = 3000;
const double EPS = 1E-10;

int n;
double X[MAXN];
double Y[MAXN];

double dist(double x, double y){
  double distance = 0;
  for (int i = 0; i < n; i++){
    double dx = x - X[i];
    double dy = y - Y[i];
    distance += sqrt(dx * dx + dy * dy);
  }
  return distance;
}

double best_for_x(double x){
  double lo = 0, hi = 1000;
  while(hi - lo > EPS){
    double y1 = (lo + lo + hi) / 3;
    double y2 = (lo + hi + hi) / 3;
    if (dist(x, y1) < dist(x, y2))
      hi = y2;
    else
      lo = y1;
  }

  return (lo + hi) / 2;
}

int main(){
  cin >> n;

  for(int i = 0; i < n; i++){
    cin >> X[i] >> Y[i];
  }

  double lo = 0, hi = 1000;
  while(hi - lo > EPS){
    double x1 = (lo + lo + hi) / 3;
    double x2 = (lo + hi + hi) / 3;

    double y1 = best_for_x(x1);
    double y2 = best_for_x(x2);

    if (dist(x1, y1) < dist(x2, y2))
      hi = x2;
    else  
      lo = x1;
  }
  double x = (lo + hi) / 2;
  double y = best_for_x(x);

  cout << dist(x, y) << endl; 
  return 0;
}


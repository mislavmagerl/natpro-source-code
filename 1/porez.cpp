#include<iostream>

using namespace std;

typedef long long ll;

const int MAXN = 1000000;

ll n, m;
int a[MAXN];

int f(ll x){
  ll sum = 0;
  for (int i = 0; i < n; i++)
    if (a[i] > x)
      sum += a[i] - x;

  return m <= sum;
}

int main(){
  cin >> n >> m;
  for (int i = 0; i < n; i++)
    cin >> a[i];
  
  ll p = 0;
  for (ll x = 1<<20; x; x = x>>1)
    if (f(p + x))
      p += x;

  cout << p << endl;
  return 0;
}

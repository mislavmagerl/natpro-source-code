#include<iostream>

using namespace std;

typedef long long ll;

const int MAXN = 100000;

int n, m;
int t[MAXN];

int f(ll x){
    ll sum = 0;
    for (int i = 0; i < n; i++)
        sum += x / t[i];

    return sum < m;
}

int main(){
    cin >> n >> m;
    for (int i = 0; i < n; i++)
        cin >> t[i];

    ll p = 0;
    for (ll x = 1L << 60; x; x = x >> 1)
        if (f(p + x))
            p += x;

    cout << p + 1 << endl;

    return 0;
}
